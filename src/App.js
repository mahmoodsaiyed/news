import './App.css';

import React from 'react'
import Navbar from './components/Navbar';
import News from './components/News';
import { BrowserRouter as Router,Route,Routes } from 'react-router-dom';
import Ex from './components/Ex'


const App=()=>  {
  
    return (
      <div>
        {/* <Ex/> */}
        <Router>
        <Navbar/>

        <Routes>
          
        <Route  path="/" element={<News key={'general'} country={'us'} category={'general'}/>}></Route>

        <Route  path="/general"element={<News key={'general'} country={'us'} category={'general'}/>}></Route>

        <Route  path="/sports"element={<News key={'sports'} country={'us'} category={'sports'}/>}></Route>

        <Route  path="/science"element={<News key={'science'} country={'us'} category={'science'}/>}></Route>

        <Route  path="/technology"element={<News key={'technology'} country={'us'} category={'technology'}/>}></Route>

        <Route  path="/health"element={<News key={'health'} country={'us'} category={'health'}/>}></Route>

        <Route  path="/business"element={<News key={'business'} country={'us'} category={'business'}/>}></Route>

        <Route  path="/entertainment"element={<News key={'entertainment'} country={'us'} category={'entertainment'}/>}></Route>

        </Routes>
        </Router>
      </div>
    )
  
}
export default App;
